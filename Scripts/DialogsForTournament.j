//! textmacro Ask takes name, p
    call Ask($name$Dialog, $p$.user, function $name$_Callback)
//! endtextmacro

globals
    // From GameScope
    constant integer MinWinsAmount = 1
    constant integer MaxWinsAmount = 15
    integer WinsAmountToEnd
    boolean SwapTokensEachRound
    constant integer DefaultWinsAmount = 3
    constant boolean DefaultTokenSwapping = true
    // Dialogs
    CustomDialog RestartDialog
    CustomDialog AskFieldSizeDialog
    CustomDialog AskWinSequenceDialog
    CustomDialog AskWhoFirstDialog
    CustomDialog AskWinsAmountDialog
    CustomDialog AskTokenSwappingDialog
    CustomDialog AskTurnTimeDialog
    // Buttons
    CustomButton RestartButton
    
    CustomButton FieldSize_3
    CustomButton FieldSize_5
    CustomButton FieldSize_15
    CustomButton FieldSize_19
    
    CustomButton WinSequence_3
    CustomButton WinSequence_4
    CustomButton WinSequence_5
    
    CustomButton SelectX
    
    CustomButton WinsAmount_1
    CustomButton WinsAmount_3
    CustomButton WinsAmount_5
    
    CustomButton Swap
    
    CustomButton TurnTime_15
    CustomButton TurnTime_30
    CustomButton TurnTime_60
    // Dialogs' messages
    constant string Greetings = "|cffffffffWelcome to Super tic-tac-toe game!|r\n\n"
    
    constant string AskFieldSize_Message = "Choose the field size"
    constant string AskFieldSize_FirstMessage = Greetings + "             " + AskFieldSize_Message
    constant string AskWinSequence_Message = "Choose the amount of tokens for victory"
    constant string AskWhoFirst_Message = "Choose your token"
    constant string AskWinsAmount_Message = "Choose the amount of wins for tournament"
    constant string AskWinsAmount_FirstMessage = "    " + Greetings + AskWinsAmount_Message
    constant string AskTokenSwapping_Message = "Swap tokens between players each round?"
    constant string AskTurnTime_Message = "Choose duration of player's turn"
    // Callbacks
    code ShowRestartDialog
    // Other
    CustomPlayer HeWhoSelectsFieldOptions
    CustomPlayer HeWhoSelectsWinsAmount
    
    integer RestartButtonClicked
    integer FinalOptionClicked = 0
    constant string WaitMessage = "Wait until your opponent completes selecting his options."
endglobals

function Ask takes CustomDialog d, player p, code callback returns nothing
    call d.prepareWithButtons()
    call d.toggleViewForPlayer(p, true)
    call d.startTimeout(callback)
endfunction

// AskTurnTime

function AskTurnTime_DefaultActions takes nothing returns nothing
    set CurrentTurnTime = DefaultTurnTime
    call StartGame(HeWhoSelectsWinsAmount)
endfunction

function AskTurnTime_Actions takes nothing returns nothing
    local CustomButton b = GetCustomButton()
    call GetCustomDialog().hide()
    if b == TurnTime_15 then
        set CurrentTurnTime = 16
    elseif b == TurnTime_30 then
        set CurrentTurnTime = 31
    elseif b == TurnTime_60 then
        set CurrentTurnTime = 61
    else
        // Space for Custom answer.
    endif
    call StartGame(HeWhoSelectsWinsAmount)
endfunction

function AskTurnTime_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call AskTurnTime_DefaultActions()
    endif
endfunction

// AskTokenSwapping

function AskTokenSwapping_Actions takes nothing returns nothing
    set SwapTokensEachRound = GetCustomButton() == Swap
    call GetCustomDialog().hide()
//! runtextmacro Ask("AskTurnTime", "HeWhoSelectsWinsAmount")
endfunction

function AskTokenSwapping_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        set SwapTokensEachRound = DefaultTokenSwapping
    //! runtextmacro Ask("AskTurnTime", "HeWhoSelectsWinsAmount")
    endif
endfunction

// AskWinsAmount

function AskWinsAmount_DefaultActions takes nothing returns nothing
    set WinsAmountToEnd = DefaultWinsAmount
//! runtextmacro Ask("AskTokenSwapping", "HeWhoSelectsWinsAmount")
endfunction

function AskWinsAmount_Actions takes nothing returns nothing
    local CustomButton b = GetCustomButton()
    call GetCustomDialog().hide()
    if b == WinsAmount_1 then
        set WinsAmountToEnd = 1
    elseif b == WinsAmount_3 then
        set WinsAmountToEnd = 3
    elseif b == WinsAmount_5 then
        set WinsAmountToEnd = 5
    else
        // Space for Custom answer.
    endif
//! runtextmacro Ask("AskTokenSwapping", "HeWhoSelectsWinsAmount")
endfunction

function AskWinsAmount_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call AskWinsAmount_DefaultActions()
    endif
endfunction

// AskWhoFirst

function AskWhoFirst_Actions takes nothing returns nothing
    local CustomButton b = GetCustomButton()
    call GetCustomDialog().hide()
    call HeWhoSelectsFieldOptions.prepare(b == SelectX)
    call HeWhoSelectsWinsAmount.prepare(b != SelectX)
    call StartGame(HeWhoSelectsFieldOptions)
endfunction

function AskWhoFirst_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call HeWhoSelectsFieldOptions.prepare(true)
        call HeWhoSelectsWinsAmount.prepare(false)
        call StartGame(HeWhoSelectsFieldOptions)
    endif
endfunction

// AskWinSequence

function AskWinSequence_ChooseNext takes nothing returns nothing
    if IsMultiplayer then
    //! runtextmacro Ask("AskWhoFirst", "HeWhoSelectsFieldOptions")
    else
        call StartGame(0)
    endif
endfunction

function AskWinSequence_DefaultActions takes nothing returns nothing
    if N >= DefaultWinSequence then
        set K = DefaultWinSequence
    else
        set K = MinFieldSize
    endif
    call AskWinSequence_ChooseNext()
endfunction

function AskWinSequence_Actions takes nothing returns nothing
    local CustomButton b = GetCustomButton()
    call GetCustomDialog().hide()
    if b == WinSequence_3 then
        set K = 3
    elseif b == WinSequence_4 then
        set K = 4
    elseif b == WinSequence_5 then
        set K = 5
    else
        // Space for Custom answer.
    endif
    call AskWinSequence_ChooseNext()
endfunction

function AskWinSequence_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call AskWinSequence_DefaultActions()
    endif
endfunction

function AskWinSequence takes nothing returns nothing
    if N == MinFieldSize then
        set K = MinFieldSize
        call AskWinSequence_ChooseNext()
        return
    endif
    // N > 3
    call AskWinSequenceDialog.prepare()
    call WinSequence_3.initButtonWithTimeInd(N < 5)
    call WinSequence_4.initButton()
    if N > 4 then
        call WinSequence_5.initButton()
        if N > 5 then
            // Space for Custom button.
        endif
    endif
    call AskWinSequenceDialog.toggleViewForPlayer(HeWhoSelectsFieldOptions.user, true)
    if IsMultiplayer then
        call AskWinSequenceDialog.startTimeout(function AskWinSequence_Callback)
    endif
endfunction

// AskFieldSize

function AskFieldSize_DefaultActions takes nothing returns nothing
    set N = DefaultFieldSize
    call AskWinSequence()
endfunction

function AskFieldSize_Actions takes nothing returns nothing
    local CustomButton b = GetCustomButton()
    call GetCustomDialog().hide()
    if b == FieldSize_3 then
        set N = 3
    elseif b == FieldSize_5 then
        set N = 5
    elseif b == FieldSize_15 then
        set N = 15
    elseif b == FieldSize_19 then
        set N = 19
    else
        // Space for Custom answer.
    endif
    call AskWinSequence()
endfunction

function AskFieldSize_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call AskFieldSize_DefaultActions()
    endif
endfunction

function AskFieldSize takes nothing returns nothing
    call AskFieldSizeDialog.prepareWithButtons()
    call AskFieldSizeDialog.toggleViewForPlayer(HeWhoSelectsFieldOptions.user, true)
    if IsMultiplayer then
        call AskFieldSizeDialog.startTimeout(function AskFieldSize_Callback)
    endif
endfunction

// Restart

function Restart takes nothing returns nothing
    call PanCameraToTimed(0., 0., 0.)
    if IsMultiplayer then
        call ClearTextMessages()
        call SetCameraQuickPosition(0., 0.)
        set bj_randDistCount = HeWhoSelectsFieldOptions
        set HeWhoSelectsFieldOptions = HeWhoSelectsWinsAmount
        set HeWhoSelectsWinsAmount = bj_randDistCount
    //! runtextmacro Ask("AskWinsAmount", "HeWhoSelectsWinsAmount")
    endif
    call AskFieldSize()
endfunction

function Restart_Callback takes nothing returns nothing
    if CustomDialog.isTimeOut() then
        call Restart()
    endif
endfunction

function Restart_Actions takes nothing returns nothing
    local CustomPlayer p = GetCustomPlayer(GetTriggerPlayer())
    local CustomDialog d = GetCustomDialog()
    local CustomButton b = GetCustomButton()
    if IsMultiplayer then
        call d.toggleViewForPlayer(p.user, false)
        if b == RestartButton then
            set RestartButtonClicked = RestartButtonClicked + 1
            if RestartButtonClicked == 2 then
                call d.hide()
                call Restart()
            else
                call p.message(WaitMessage)
            endif
        endif
    else
        call d.hide()
        if b == RestartButton then
            call Restart()
        endif
    endif
endfunction

function ShowRestartDialogFunc takes nothing returns nothing
    call EnableUserControl(true)
    call ClearField()
    call RestartDialog.prepareWithButtons()
    call RestartDialog.toggleViewForPlayer(LocalPlayer, true)
    if IsMultiplayer then
        set RestartButtonClicked = 0
        set FinalOptionClicked = 0
        call RestartDialog.startTimeout(function Restart_Callback)
    endif
endfunction

function InitDialogs takes nothing returns nothing
    // Callbacks
    set ShowRestartDialog = function ShowRestartDialogFunc
    // Dialogs
    // Restart
    set RestartDialog = CustomDialog.create(null, function Restart_Actions)
    set RestartButton = RestartDialog.addButton(false, "Restart", true)
    call RestartDialog.addButton(true, "Leave", false)
    // AskFieldSize
    set AskFieldSizeDialog = CustomDialog.create(AskFieldSize_FirstMessage, function AskFieldSize_Actions)
    set FieldSize_3 = AskFieldSizeDialog.addButton(false, "3x3", false)
    set FieldSize_5 = AskFieldSizeDialog.addButton(false, "5x5", false)
    set FieldSize_15 = AskFieldSizeDialog.addButton(false, "15x15", true)
    set FieldSize_19 = AskFieldSizeDialog.addButton(false, "19x19", false)
    // Space for Custom button.
    // AskWinSequence
    set AskWinSequenceDialog = CustomDialog.create(AskWinSequence_Message, function AskWinSequence_Actions)
    set WinSequence_3 = AskWinSequenceDialog.addButton(false, "3", false)
    set WinSequence_4 = AskWinSequenceDialog.addButton(false, "4", false)
    set WinSequence_5 = AskWinSequenceDialog.addButton(false, "5", true)
    // Space for Custom button.
    if IsMultiplayer then
        // AskWhoFisrt
        set AskWhoFirstDialog = CustomDialog.create(AskWhoFirst_Message, function AskWhoFirst_Actions)
        set SelectX = AskWhoFirstDialog.addButton(false, "Select 'X'", true)
        call AskWhoFirstDialog.addButton(false, "Select 'O'", false)
        // AskWinsAmount
        set AskWinsAmountDialog = CustomDialog.create(AskWinsAmount_FirstMessage, function AskWinsAmount_Actions)
        set WinsAmount_1 = AskWinsAmountDialog.addButton(false, "1", false)
        set WinsAmount_3 = AskWinsAmountDialog.addButton(false, "3", true)
        set WinsAmount_5 = AskWinsAmountDialog.addButton(false, "5", false)
        // Space for Custom button.
        // AskTokenSwapping
        set AskTokenSwappingDialog = CustomDialog.create(AskTokenSwapping_Message, function AskTokenSwapping_Actions)
        set Swap = AskTokenSwappingDialog.addButton(false, "Yes", true)
        call AskTokenSwappingDialog.addButton(false, "No", false)
        // AskTurnTime
        set AskTurnTimeDialog = CustomDialog.create(AskTurnTime_Message, function AskTurnTime_Actions)
        set TurnTime_15 = AskTurnTimeDialog.addButton(false, "15 seconds", true)
        set TurnTime_30 = AskTurnTimeDialog.addButton(false, "30 seconds", false)
        set TurnTime_60 = AskTurnTimeDialog.addButton(false, "1 minute", false)
        // Space for Custom button.
        // Other
        set HeWhoSelectsWinsAmount = SecondPlayer
    endif
    set HeWhoSelectsFieldOptions = FirstPlayer
endfunction
